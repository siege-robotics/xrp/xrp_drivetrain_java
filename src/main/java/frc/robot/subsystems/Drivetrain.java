// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.kinematics.MecanumDriveKinematics;
import edu.wpi.first.math.kinematics.MecanumDriveWheelSpeeds;
import edu.wpi.first.util.sendable.SendableRegistry;
import edu.wpi.first.wpilibj.BuiltInAccelerometer;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.xrp.XRPGyro;
import edu.wpi.first.wpilibj.xrp.XRPMotor;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import frc.robot.Constants;
import frc.robot.Constants.DriveConstants;

public class Drivetrain extends SubsystemBase {
  
  // The XRP has the left and right motors set to
  // channels 0 and 1 respectively
  private final XRPMotor m_frontLeftMotor = new XRPMotor(0);
  private final XRPMotor m_frontRightMotor = new XRPMotor(1);
  private final XRPMotor m_backLeftMotor = new XRPMotor(2);
  private final XRPMotor m_backRightMotor = new XRPMotor(3);

  private final MotorControllerGroup m_leftMotorGroup = new MotorControllerGroup(m_frontLeftMotor, m_backLeftMotor);
  private final MotorControllerGroup m_rightMotorGroup = new MotorControllerGroup(m_frontRightMotor, m_backRightMotor);

  // The XRP has onboard encoders that are hardcoded
  // to use DIO pins 4/5 and 6/7 for the left and right
  private final Encoder m_frontLeftEncoder = new Encoder(4, 5);
  private final Encoder m_frontRightEncoder = new Encoder(6, 7);
  private final Encoder m_backLeftEncoder = new Encoder(8, 9);
  private final Encoder m_backRightEncoder = new Encoder(10, 11);  

  // Create SimpleMotorFeedforwards with gains kS, kV, and kA
  SimpleMotorFeedforward m_frontLeftFf = new SimpleMotorFeedforward(DriveConstants.kS, DriveConstants.kV, DriveConstants.kA);
  SimpleMotorFeedforward m_frontRightFf = new SimpleMotorFeedforward(DriveConstants.kS, DriveConstants.kV, DriveConstants.kA);
  SimpleMotorFeedforward m_backLeftFf = new SimpleMotorFeedforward(DriveConstants.kS, DriveConstants.kV, DriveConstants.kA);
  SimpleMotorFeedforward m_backRightFf = new SimpleMotorFeedforward(DriveConstants.kS, DriveConstants.kV, DriveConstants.kA);

  // Creates PIDControllers with gains kP, kI, and kD
  PIDController m_frontLeftPid = new PIDController(DriveConstants.kP, DriveConstants.kI, DriveConstants.kD);
  PIDController m_frontRightPid = new PIDController(DriveConstants.kP, DriveConstants.kI, DriveConstants.kD);
  PIDController m_backLeftPid = new PIDController(DriveConstants.kP, DriveConstants.kI, DriveConstants.kD);
  PIDController m_backRightPid = new PIDController(DriveConstants.kP, DriveConstants.kI, DriveConstants.kD);

  // Locations of the wheels relative to the robot center.
  private final Translation2d m_frontLeftLocation = new Translation2d(DriveConstants.kTrackWidthMeters/2.0, DriveConstants.kTrackLengthMeters/2.0);
  private final Translation2d m_frontRightLocation = new Translation2d(DriveConstants.kTrackWidthMeters/2.0, -DriveConstants.kTrackLengthMeters/2.0);
  private final Translation2d m_backLeftLocation = new Translation2d(-DriveConstants.kTrackWidthMeters/2.0, DriveConstants.kTrackLengthMeters/2.0);
  private final Translation2d m_backRightLocation = new Translation2d(-DriveConstants.kTrackWidthMeters/2.0, -DriveConstants.kTrackLengthMeters/2.0);

  // Creating my kinematics object using the wheel locations.
  DifferentialDriveKinematics m_kinematicsDiff = new DifferentialDriveKinematics(DriveConstants.kTrackWidthMeters);
  MecanumDriveKinematics m_kinematicsMec = new MecanumDriveKinematics(m_frontLeftLocation, m_frontRightLocation, m_backLeftLocation, m_backRightLocation);

  // Set up the differential drive controller
  private final DifferentialDrive m_diffDrive = new DifferentialDrive(m_leftMotorGroup::set, m_rightMotorGroup::set);

  // Set up the mechanum drive controller
  private final MecanumDrive m_mecDrive = new MecanumDrive(m_frontLeftMotor::set, m_backLeftMotor::set, m_frontRightMotor::set, m_backRightMotor::set);      

  // Set up the XRPGyro
  private final XRPGyro m_gyro = new XRPGyro();

  // Set up the BuiltInAccelerometer
  private final BuiltInAccelerometer m_accelerometer = new BuiltInAccelerometer();

  /** Creates a new Drivetrain. */
  public Drivetrain() {
    SendableRegistry.addChild(m_diffDrive, m_frontLeftMotor);
    SendableRegistry.addChild(m_diffDrive, m_frontRightMotor);
    SendableRegistry.addChild(m_diffDrive, m_backLeftMotor);
    SendableRegistry.addChild(m_diffDrive, m_backRightMotor);     

    SendableRegistry.addChild(m_mecDrive, m_frontLeftMotor);
    SendableRegistry.addChild(m_mecDrive, m_frontRightMotor);
    SendableRegistry.addChild(m_mecDrive, m_backLeftMotor);
    SendableRegistry.addChild(m_mecDrive, m_backRightMotor);        

    // We need to invert one side of the drivetrain so that positive voltages
    // result in both sides moving forward. Depending on how your robot's
    // gearbox is constructed, you might have to invert the left side instead.
    m_frontLeftMotor.setInverted(true);
    m_backLeftMotor.setInverted(true);
    m_frontRightMotor.setInverted(false);
    m_backRightMotor.setInverted(false);

    // Use meters as unit for encoder distances
    m_frontLeftEncoder.setDistancePerPulse((Math.PI * Constants.DriveConstants.kWheelDiameterMeters) / Constants.DriveConstants.kCountsPerRevolution);
    m_frontRightEncoder.setDistancePerPulse((Math.PI * Constants.DriveConstants.kWheelDiameterMeters) / Constants.DriveConstants.kCountsPerRevolution);
    m_backLeftEncoder.setDistancePerPulse((Math.PI * Constants.DriveConstants.kWheelDiameterMeters) / Constants.DriveConstants.kCountsPerRevolution);
    m_backRightEncoder.setDistancePerPulse((Math.PI * Constants.DriveConstants.kWheelDiameterMeters) / Constants.DriveConstants.kCountsPerRevolution);    
    resetEncoders();
  }

  public void arcadeDrive(double xaxisSpeed, double zaxisRotate) {
    m_diffDrive.arcadeDrive(xaxisSpeed, zaxisRotate);
  }

  public void cartesianDrive(double xaxisSpeed, double yaxisSpeed, double zaxisRotate) {
    m_mecDrive.driveCartesian(xaxisSpeed, yaxisSpeed, zaxisRotate);
    }

  public void driveWithFeedforwardPid(double xaxisSpeed, double yaxisSpeed, double zaxisRotate) {
    
    // initialize variables
    double frontLeftSpeed = 0.0;
    double frontRightSpeed = 0.0;
    double backLeftSpeed = 0.0;
    double backRightSpeed = 0.0;

    // Create chassisSpeeds object
    ChassisSpeeds chassisSpeeds = new ChassisSpeeds(xaxisSpeed, yaxisSpeed, zaxisRotate);

    // Convert to wheel speeds
    MecanumDriveWheelSpeeds wheelSpeeds = m_kinematicsMec.toWheelSpeeds(chassisSpeeds);

    // Get the individual wheel speeds
    frontLeftSpeed = wheelSpeeds.frontLeftMetersPerSecond;
    frontRightSpeed = wheelSpeeds.frontRightMetersPerSecond;
    backLeftSpeed = wheelSpeeds.rearLeftMetersPerSecond;
    backRightSpeed = wheelSpeeds.rearRightMetersPerSecond;
    
    // Get Feedforward Output
    double frontLeftFf = m_frontLeftFf.calculate(frontLeftSpeed); 
    double frontRightFf = m_frontRightFf.calculate(frontRightSpeed); 
    double backLeftFf = m_backLeftFf.calculate(backLeftSpeed); 
    double backRightFf = m_backRightFf.calculate(backRightSpeed); 

    // Get PID's Output
    double frontLeftPid = m_frontLeftPid.calculate(m_frontLeftEncoder.getRate());
    double frontRightPid = m_frontRightPid.calculate(m_frontRightEncoder.getRate());
    double backLeftPid = m_backLeftPid.calculate(m_backLeftEncoder.getRate());
    double backRightPid = m_backRightPid.calculate(m_backRightEncoder.getRate());

    // Send to Motors
    m_frontLeftMotor.set(frontLeftFf + frontLeftPid);
    m_frontRightMotor.set(frontRightFf + frontRightPid);
    m_backLeftMotor.set(backLeftFf + backLeftPid);
    m_backRightMotor.set(backRightFf + backRightPid);
  }    

  public void resetEncoders() {
    m_frontLeftEncoder.reset();
    m_frontRightEncoder.reset();
    m_backLeftEncoder.reset();
    m_backRightEncoder.reset();
  }

  public int getFrontLeftEncoderCount() {
    return m_frontLeftEncoder.get();
  }

  public int getbackLeftEncoderCount() {
    return m_backLeftEncoder.get();
  }

  public int getFrontRightEncoderCount() {
    return m_frontRightEncoder.get();
  }

  public int getBackRightEncoderCount() {
    return m_backRightEncoder.get();
  }

  public double getFrontLeftDistanceMeters() {
    return m_frontLeftEncoder.getDistance();
  }

  public double getBackLeftDistanceMeters() {
    return m_backLeftEncoder.getDistance();
  }

  public double getFrontRightDistanceMeters() {
    return m_frontRightEncoder.getDistance();
  }

  public double getBackRightDistanceMeters() {
    return m_backRightEncoder.getDistance();
  }

  public double getAverageLeftDistanceMeters() {
    return (getFrontLeftDistanceMeters() + getBackLeftDistanceMeters()) / 2.0;
  }

  public double getAverageRightDistanceMeters() {
    return (getFrontRightDistanceMeters() + getBackRightDistanceMeters()) / 2.0;
  }

  public double getAverageFrontDistanceMeters() {
    return (getFrontLeftDistanceMeters() + getFrontRightDistanceMeters()) / 2.0;
  }

  public double getAverageDistanceMeters() {
    return (getAverageLeftDistanceMeters() + getAverageRightDistanceMeters()) / 2.0;
  }

  /**
   * The acceleration in the X-axis.
   *
   * @return The acceleration of the XRP along the X-axis in Gs
   */
  public double getAccelX() {
    return m_accelerometer.getX();
  }

  /**
   * The acceleration in the Y-axis.
   *
   * @return The acceleration of the XRP along the Y-axis in Gs
   */
  public double getAccelY() {
    return m_accelerometer.getY();
  }

  /**
   * The acceleration in the Z-axis.
   *
   * @return The acceleration of the XRP along the Z-axis in Gs
   */
  public double getAccelZ() {
    return m_accelerometer.getZ();
  }

  /**
   * Current angle of the XRP around the X-axis.
   *
   * @return The current angle of the XRP in degrees
   */
  public double getGyroAngleX() {
    return m_gyro.getAngleX();
  }

  /**
   * Current angle of the XRP around the Y-axis.
   *
   * @return The current angle of the XRP in degrees
   */
  public double getGyroAngleY() {
    return m_gyro.getAngleY();
  }

  /**
   * Current angle of the XRP around the Z-axis.
   *
   * @return The current angle of the XRP in degrees
   */
  public double getGyroAngleZ() {
    return m_gyro.getAngleZ();
  }

  /** Reset the gyro. */
  public void resetGyro() {
    m_gyro.reset();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
