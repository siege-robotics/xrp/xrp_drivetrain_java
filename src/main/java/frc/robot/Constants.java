// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.units.Distance;
import edu.wpi.first.units.Unit;
import edu.wpi.first.units.Units;
import edu.wpi.first.units.Velocity;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
  public static class OperatorConstants {
    public static final int kDriverControllerPort = 0;
  }

  public static class DriveConstants {
    // original XRP    
    // public static final double kGearRatio = (30.0 / 14.0) * (28.0 / 16.0) * (36.0 / 9.0) * (26.0 / 8.0); // 48.75:1
    // public static final double kCountsPerMotorShaftRev = 12.0;
    // public static final double kCountsPerRevolution = kCountsPerMotorShaftRev * kGearRatio; // 585.0
    // public static final double kWheelDiameter = 60; // 60_mm
    // public static final double kTrackWidth = 155; //155_mm
    // public static final double kTrackLength = 155; //155_mm

    // public static final double kP = 0.005;
    // public static final double kI = 0.0;
    // public static final double kF = 0.0;

    // Highwonder Mecanum
    public static final double kGearRatio = 120;
    public static final double kCountsPerMotorShaftRev = 12.0;
    public static final double kCountsPerRevolution = kCountsPerMotorShaftRev * kGearRatio; // 585.0
    public static final double kWheelDiameterMeters = Units.Meters.convertFrom(65.0, Units.Millimeters); // 65_mm
    public static final double kTrackWidthMeters = Units.Meters.convertFrom(127.0, Units.Millimeters); //155_mm
    public static final double kTrackLengthMeters = Units.Meters.convertFrom(115.0, Units.Millimeters); //155_mm 
    
    public static final double kS = 0.0;
    public static final double kV = 0.0;
    public static final double kA = 0.0;

    public static final double kP = 0.0;
    public static final double kI = 0.0;
    public static final double kD = 0.0;

    // // Black Mecanum
    // public static final double kGearRatio = 48.75; // 48.75:1
    // public static final double kCountsPerMotorShaftRev = 12.0;
    // public static final double kCountsPerRevolution = kCountsPerMotorShaftRev * kGearRatio; // 585.0
    // public static final double kWheelDiameterMeters = Units.Meters.convertFrom(61.0, Units.Millimeters); // 65_mm
    // public static final double kTrackWidthMeters = Units.Meters.convertFrom(114.0, Units.Millimeters); //155_mm
    // public static final double kTrackLengthMeters = Units.Meters.convertFrom(135.0, Units.Millimeters); //155_mm 
    
    // public static final double kS = 0.0;
    // public static final double kV = 0.0;
    // public static final double kA = 0.0;

    // public static final double kP = 0.0;
    // public static final double kI = 0.0;
    // public static final double kD = 0.0;
  }
}
